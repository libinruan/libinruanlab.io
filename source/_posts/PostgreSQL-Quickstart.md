---
title: PostgreSQL Quickstart
tags: []
categories: []
secret: False
comments: true
date: 2019-06-03 20:15:41
sticky:
---

PostgreSQL command line operations for my future reference.

<!--more-->

Launch PostgreSQL through SQL Shell

![](https://i.postimg.cc/Y9fv89wH/2021-06-03-at-20-17-35.png)

Enter your previously set password, you should see

![](https://i.postimg.cc/0yDP74vg/2021-06-03-at-20-19-04.png)

Inside the database called **postgres**, list all the databases available in this server, type:

```txt
postgres=# \l
```

Create a new database

```txt
postgres=# CREATE DATABASE new_database;
```

![](https://i.postimg.cc/L822BT8n/2021-06-03-at-20-24-11.png)

Close the **postgres** database. Relaunch the server through SQL Shell. This time use the newly created database **new_database** directly.

![](https://i.postimg.cc/0jzbvCw8/2021-06-03-at-20-27-28.png)

List all the tables inside the database.

```txt
new_database=# \d
```

Create a new table

```txt
CREATE TABLE person (
id BIGSERIAL NOT NULL PRIMARY KEY,
name VARCHAR(100) NOT NULL,
country VARCHAR(50) NOT NULL);
```

![](https://i.postimg.cc/HnN3W3v3/2021-06-03-at-20-32-42.png)

```sql
INSERT INTO person (name, country) VALUES ('Amigoscode', 'UK');
INSERT INTO person (name, country) VALUES ('Sarah', 'Albania');
INSERT INTO person (name, country) VALUES ('Julio', 'Argentina');
```

![](https://i.postimg.cc/MTvdChdX/2021-06-03-at-20-37-43.png)

```sql
UPDATE person SET name = 'Antonio' WHERE id = 3;
```

```sql
DELETE FROM person WHERE id = 2;
```

Toggle expanded display:

```txt
new_database=# \x 
```

To execute query from a file:

```txt
new_database=# \i <the-path-to-sql-file>
```

