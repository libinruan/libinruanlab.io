---
title: Distribution of Sum of I.I.D. Uniformly Distributed Random Variables
tags: []
categories: []
secret: false
comments: true
date: 2020-06-04 23:30:33
sticky:
---

Questions like finding the probability distribution of a function of a set of simple random variables is frequently raised in technical interviews. In this post I derived the distribution of the sum of i.i.d. random variables uniformly distributed and apply the finding to solve a commonly seen probability exam question. <!--more-->

# Distribution of Multiple I.I.D. Random Variables

![](https://i.postimg.cc/85cP19R3/2021-06-12-at-23-33-56.png)

# Application of Multiple I.I.D. Random Variables

![](https://i.postimg.cc/wTLJ8RTN/2021-06-12-at-23-35-30.png)

![](https://i.postimg.cc/NGWTzkYY/2021-06-12-at-23-34-27.png)

# References

1. "Mathematical Statistics with Applications," by Dennis Wackerly et al., 7th Edition, p. 304.
2. "A First Course in Probability," by Sheldon Ross, 9th Edition, p. 239.
3. "Finding the distribution of the sum of three independent uniform random variable," StackOverflow ([link](https://math.stackexchange.com/questions/2631501/finding-the-distribution-of-the-sum-of-three-independent-uniform-random-variable))

