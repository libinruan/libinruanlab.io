---
title: Automate Ngrok Tunnel Creation with a Click
date: 2021-02-27 11:14:35
tags: [SSH, ngrok, TPU, TensorBoard]
categories: [cloud computing]
comments: true
sticky:
---

As a mediocre amateur of online data science competition, I like to work on Google Colab for its provision of free TPU and GPU computing support.  I like to work locally with my favorite coding editor VS Code on a remote Colab instance. The only thing that bothers me is <!--more--> the overheads spent on manually setting up Ngrok tunnels for my every SSH connection. Now, I created a package to resolve this issue. 

<div class="image" 
     style="display:table; text-align:center; margin:30px auto;">   
  <img src="https://www.kidscodecs.com/wp-content/uploads/2013/11/puzzles-7-bridges-wikipedia.jpg" 
       alt="MAP BY MERIAN-ERBEN (1652) ON WIKIPEDIA" 
       style="width:600px; border=0;"/>
  <div style="display:table-caption; caption-side:bottom;"> 
 </div>
</div>



# sshColab
sshColab is a Python module to facilitate remote access to Google Colaboratory
(Colab) through Secure Shell (SSH) connections, secured by a third-party
software, ngrok. sshColab automates the tedious routine to set up ngrok
tunnels needed for TPU runtime applications, services like TensorBoard, etc.

# Prerequisites
- [ngrok](https://ngrok.com/) tunnel authtoken.
- Google account to access a [Colab](https://colab.research.google.com/notebooks/intro.ipynb) notebook.
- Local code editors such as VS Code or PyCharm to make the most of coding on Colab.
# Usage
1. Launch a Colab notebook. Choose a runtime type you prefer.
2. Install SSH-Colab. Type and run the following command in a notebook cell:
   ```shell
   !pip install sshColab
   ```
3. Initiate the establishment of tunnels:
   ```python
   import sshColab
   sshColab.connect()
   ```
   The default TensorBoard log directory is `/log/fit`. You can reset it by
   passing into `connect()` the new value `LOG_DIR=/new/path/to/log`.
4. Retrieve information that is used for establishing the SSH connection:
   ```python
   sshColab.info()
   ```
   If you are using non-TPU runtimes, the setup instruction of TPU resolver is
   ignored.
5. To disable ngrok tunnels created, run the command below:
   ```python
   SSHColab.kill()
   ```

# Quickstart
A quickstart Colab notebook template is provided in the link below. Users can
find a simple end-to-end application starting from sshColab installation, SSH
tunnel creation, to the use of TensorBoard after training a 3-layer MNIST
convolutional neural network. 

Here is the notebook: [![Open In Colab](https://colab.research.google.com/assets/colab-badge.svg)](https://colab.research.google.com/drive/1TKM6Zqk4oY8RrFqGzrM-QEt92iQ0zxwm?usp=sharing) 

What's missed in this quickstart is how to may our way to Colab instances from
local machines. The reference listed below can be a start point for interested
users:

1. [Remote development over SSH on local VS Code](https://code.visualstudio.com/docs/remote/ssh-tutorial)
2. [Run SSH terminal on local PyCharm](https://www.jetbrains.com/help/pycharm/running-ssh-terminal.html)

