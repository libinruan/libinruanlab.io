---
title: Running SQL Queries from VS Code
tags: [SQL]
categories: []
secret: false
comments: true
date: 2020-06-03 16:46:48
sticky:
---

In this post I show the methods to run SQL scripts in Microsoft Visual Studio Code. The SQL languages I cover include PostgreSQL and MS SQL server. <!--more-->



# MS SQL Server

## Step 1. Checking if your SQL Server is running.

Click **Windows** key. Type `services.msc` and click enter. In the pop-up window, search for something prefixed by **SQL Server** under the **Name** column. Check if the server status reads **Running**.

![](https://i.postimg.cc/63mhqq1t/2021-06-03-at-16-55-33.png)

If the server is in service, move on to next step. Otherwise, launch **SQL Server Management Studio** (SSMS) to confirm the accessibility of our working server.

![](https://i.postimg.cc/x1Cw8x52/2021-06-03-at-17-00-27.png)

Click **OK** in the **Connect to Server** dialogue window. Then you should see the list of databases in your Windows machine like this:

![](https://i.postimg.cc/vHQBqyYt/2021-06-03-at-17-03-19.png)

We will work on the database named **01_Facebook** specifically later. Now we can move onto next step.

## Step 2. Installing the VS Code extension mssql.

![](https://i.postimg.cc/1z56FFTw/2021-06-03-at-17-07-50.png)

## Step 3. Connecting VS Code to databases.

In VS Code, click on the **SQL Server** icon on the activity bar.

![](https://i.postimg.cc/QM1yPRSj/2021-06-03-at-17-11-54.png)

Click **Add Connection** and type `localhost` into the blank box that reads "Server name or ADO.NET connection string."

![](https://i.postimg.cc/mg5zcrxg/2021-06-03-at-17-12-49.png)

Then type the name of database you like to connect yourself to. In our example, we use `01_Facebook` as mentioned in Step 1. And then choose `Integrated`. 

> If you want to work across databases, then just enter `master`. 

![](https://i.postimg.cc/gjj5M4Jp/2021-06-03-at-17-17-20.png)

Then enter a display name for the connection profile  you prefer. In our example, I use `My-SQL-Playground`.

![](https://i.postimg.cc/h47PP0s6/2021-06-03-at-17-19-01.png)

If everything runs smoothly, you should see in the side bar something like this:

![](https://i.postimg.cc/jdFXQtPL/2021-06-03-at-17-20-29.png)

Under the profile name **My-SQL-Playground**, I can see two tables names **dbo.advertiser** and **dbo.dailypay** that are contained in my **01_Facebook** database hosted in my Windows machine.

> If you enter `master` instead of `01_Facebook`, then you should see a complete list of databases shown:
>
> ![](https://i.postimg.cc/ZKcdryDT/2021-06-03-at-17-51-58.png)

## Step 4. Writing and running a query

Create a new file with filename extension `.sql`. And test if we can run a query successfully. You may see the following warning message, just click Cancel to ignore it.

![](https://i.postimg.cc/vZ0qcktv/2021-06-03-at-17-31-40.png)

To execute a SQL query, press `Ctrl + Shift + E` or simply right-click and choose **Execute Query**. Choose a suitable option from the drop-down list. In our example, the option is **My-SQL-Playground**.

![](https://i.postimg.cc/4N9wJnYs/2021-06-03-at-17-35-12.png)

I can see the data stored in the working table. Done!

![](https://i.postimg.cc/2jcvPYTm/2021-06-03-at-17-37-38.png)

# PostgreSQL

## Step 1. Installing the VS Code extension PostgreSQL

![](https://i.postimg.cc/jjDDG7D6/2021-06-03-at-21-57-45.png)

Be sure the extension is enabled.

## Step 2. The SQL Server extension has to be disabled.

![](https://i.postimg.cc/QxNx2TPB/2021-06-03-at-22-02-13.png)

## Step 3. Connecting to Postgres server.

In VS Code, press `Ctrl + Shift + P`. Choose `PostgresSQL: New Query`.

![](https://i.postimg.cc/MH75V4xG/2021-06-03-at-22-11-26.png)

If the profile is not listed in the drop-down list, choose **Create Connection Profile** to create a new one.

![](https://i.postimg.cc/XJJXwybB/2021-06-03-at-22-17-42.png)

The name of the profile has to match the working Postgres server shown in the pgAdmin4 window. In the current case, it is **127.0.0.1**.

![](https://i.postimg.cc/hjWcGzGq/2021-06-03-at-22-20-51.png)

Type the profile name into the blank box that asks for server name.

![](https://i.postimg.cc/W15dtnRc/2021-06-03-at-22-20-13.png)



![](https://i.postimg.cc/BQ93c15L/2021-06-03-at-22-23-48.png)

![](https://i.postimg.cc/L4v5xDXY/2021-06-03-at-22-23-18.png)

Right-click the database we like to work on. Click **Properties** to see the configuration of username and port.

![](https://i.postimg.cc/TwdzHr7Q/2021-06-03-at-22-25-42.png)

![](https://i.postimg.cc/C1b4DtHQ/2021-06-03-at-22-27-14.png)

![](https://i.postimg.cc/XJ6fVLSc/2021-06-03-at-22-27-34.png)

![](https://i.postimg.cc/Hx0QjMTG/2021-06-03-at-22-28-11.png)

![](https://i.postimg.cc/5ynLSMHn/2021-06-03-at-22-29-38.png)

## Step 4. Writing and running a query.

Create a SQL file and save it. Then execute the file by right-clicking your mouse and choose **Execute Query**.

![](https://i.postimg.cc/QM3CCyCt/2021-06-03-at-22-31-59.png)

![](https://i.postimg.cc/FRFKgkjX/2021-06-03-at-22-32-49.png)

After entering your password and pressing **Enter**, you should see the outcome of the query shown in a new panel similar to the following table:

![](https://i.postimg.cc/j2pK9DJ5/2021-06-03-at-22-34-31.png)

# VS Code Extension: Database Client

Database Client for Visual Studio Code supports databases MySQL/MariaDB, Microsoft SQL Server, PostgreSQL, SQLite, MongoDB, Redis, and ElasticSearch. Here is the procedure to install the extension and create a new connection to a SQL database.

Step 1. Install the extension.

![](https://i.postimg.cc/mrBnqSrD/2021-07-06-at-21-40-23.png)

Two new extension icons will be listed on the activity bar.

Step 2. Launch `pgAdmin 4` if your working database is chosen to be PostgreSQL.

![](https://i.postimg.cc/g2y1c7Xd/2021-07-06-at-21-59-37.png)

Put in a password you prefer to be used in the newly installed extension later.

![](https://i.postimg.cc/qqd5dXJg/2021-07-06-at-22-00-04.png)

![](https://i.postimg.cc/3r9DDXVT/2021-07-06-at-22-13-29.png)

Expand the **Servers** line. And choose the server you like to make a connection with. For example, click on `127.0.0.1` and the password request window will pop up. Enter the password you just made up.

![](https://i.postimg.cc/TYFSfDRV/2021-07-06-at-22-17-47.png)

Click the following labels in order: **Databases** > **Schemas** > **Tables**.

Step 3. Click the Database Client extension icon to create the connection to the postgreSQL database in this case. And then click on the "cross" button in the **EXPLOERE: DATABASE**. 

![](https://i.postimg.cc/CxXNytqh/2021-07-06-at-22-47-35.png)

Then you should see the tables in the database.

![](https://i.postimg.cc/JhYX9Ydr/2021-07-06-at-23-00-19.png)

After clicking on **Open Query**, then you can write queries by calling tables under this database.

![](https://i.postimg.cc/y6MbMvwy/2021-07-06-at-23-02-58.png)

To run the script, just click on the triangle next to **Run SQL**.

