---
title: Git101 Defunct Gitignore Caused By Late Creation
tags: [Git]
categories: []
secret: true
comments: true
date: 2020-02-27 23:30:07
sticky:
---


The files/folder in our  version control will not  delete themselves just because we  added them to the `.gitignore`. They are already in the repository and we have to **remove** them. 

**IMPORTANT!!** Be mindful before we proceed to the real solution, remember to `commit` everything we've changed before we do it! Type and run:
```shell
git rm -rf --cached .
git add .
```
This removes all files from the repository and adds them back (this time respecting the rules in our newly created `.gitignore` file).


# Reference

1. [Gitignore not working](https://stackoverflow.com/questions/25436312/gitignore-not-working)



