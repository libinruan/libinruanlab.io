---
title: SSH101 How to Use SSH to Access a Linux Machine from Windows
tags: [SSH]
categories: []
secret: true
comments: true
date: 2019-02-28 15:31:19
sticky:
---



## General routine

1. Installing **OpenSSH** on your Linux machine

   OpenSSH is to my knowledge the SSH server of choice in Linux land.

   Prerequisites: 

   - You have super-user/administrative permissions on the Linux machine, and 
   - you are familiar with the way Debian-flavored Linux accepts to find and install software packages by using the `apt install` command.

2. Installing the SSH client for Windows

   The most common SSH client for windows is Putty, an open source terminal emulator which can act as a client for SSH, Telent, and other protocols.

3. Use PuTTYGen to create a public/private key pair for your Windodws client machine

4. Configure PuTTY for the initial login

5. Add Your Public Key to the Linux Machine at `~/.ssh`

6. Use PyTTY to establish the connection



# Set Permission on Keys File to Owner/Read-Only

 Protect the public key file on Linux machine so that we don’t accidentally modify or delete it. Navigate into the **.ssh** directory, and type the following command into the Bash terminal:

   ```shell
   $ chmod 400 authorized_keys
   ```

This allows only the current user to have read-only permissions on the public SHH key file (We can instead make the file writeable for themselves again by using `chmod 700`).

# Disable Password Authentication

Once we have a working key-based authentication scheme, we have no more need for the less-than-secure password-only security we used previously. Our next step could be to edit the OpenSSH configuration file on our Linux machine to ONLY accept key-based authentication. 

1. Make a backup copy of the configuration file. Save it to home directory.

   ```shell
   $ mkdir ~/ssh_config_backup
   $ sudo cp /etc/ssh/sshd_config ~/ssh_config_backup
   ```
   
2. Add/change the value of `PasswordAuthentication` to **no** in the original config file:

   ```shell
   $ sudo nano /etc/ssh/sshd_config
   ```

   In the Nano editor, uncomment and change the target field to:

   ```shell
   PasswordAuthentication no
   ```

# Disable Password Authentication Modules

After disabling `PasswordAuthentication`, if the previous configuration file contains this variable `UsePAM`, then uncomment the corresponding line and change the value to:

```shell
UsePAM no
```





References:

| [1]  | [How to Use SSH to Access a Linux Machine from Windows](https://www.codeproject.com/Articles/497728/HowplustoplusUseplusSSHplustoplusAccessplusaplusLi) |
| ---- | ------------------------------------------------------------ |
|      |                                                              |

