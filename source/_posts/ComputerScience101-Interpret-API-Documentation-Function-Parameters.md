---
title: ComputerScience101 Interpret API Documentation Function Parameters
tags: [API]
categories: []
secret: true
comments: true
date: 2020-03-01 11:27:14
sticky:
---

A visual mnemonic to keep in your mind for interpreting API documentation function parameters is 

```shell
required [optional] <required> 
```

Simply put, square brackets ( [] ) around an argument indicate that the argument is optional, while angle braces is used for mandatory parameters except the case that angle braces were enclosed by outer bracket braces such as:

```shell
[<optional>, <but both needed>]
```

 which means both of the parameters are optional. But if the second parameter is specified, the first parameters becomes required.