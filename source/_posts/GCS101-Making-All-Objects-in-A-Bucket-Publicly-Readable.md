---
title: GCS101 Making Objects in A Bucket Publicly Readable
tags: [GCS]
categories: []
secret: true
comments: true
date: 2019-03-02 16:07:44
sticky:
---

When creating a Google Cloud Storage (GCS) Bucket, the interactive setup window prompts us to choose whether our objects in the bucket will be made public at fine-grained level or uniform level. This question always makes me feel confused because I am not an active GCS user. It tends to be the case that I forgot the answer I googled for last time I created a new GCS bucket. Anyway, here is a short post for my future reference. Hope you can find it useful too.

Personally I choose the Fine-grained access control when the question is prompted for convenience because you are allowed to change the readibility of a bucket from Fine-granted level to Uniform level if necessary later on.



# Making individual objects publicly readable

After uploading your objects to a certain object in the bucket:
1. Check the **Access Control** column of the bucket.
2. Click on the name of the bucket containing the object
3. Navigate to the object if it's in a subdirectory
4. Click the **more actions** menu (an icon with three dots aligned vertically) associated with the object you want to make public
5. Select **Edit permissions** from the drop-down menu. If the option doesn't exist, you may mistakenly have **Uniform** access control enabled.
6. In the overlay that appears, click the **+ Add entry** button.
7. Add a permission for **allUsers**:
   1. Select **Public** for the Entiry.
   2. Select **allUsers** for the Name.
   3. Select **Reader** for the Access.
8. Click **Save**.

Once shared publicly, a **link** icon appears in the public access column for you to get the URL for the object.

# Making all objects in a bucket publicly readable

If you did not choose **Uniform** access control during the bucket creation process. Here is the remedy to set the bucket-wide access control to Uniform level:

1. In the list of buckets, click on the name of the bucket that you want to make public.
2. Select the **Permissions** tab near the tp of the page.
3. Click the **Add** button when the Permissions table is displayed as veiwed by `Members`.
4. In the **New members** field, enter **allUsers**.
5. In the **Select a role** drop down menu, select the **Cloud Storage** sub-menu, and click the **Storage Object Viewers** option.
6. Click **Save**.

# Can Access Control Be Set Back to Fine Grained?

The answer is NO. If you choose Uniform level of access control in the bucket creation webpage, trying to **reset** an individual object's accessibility would lead to a warning message like this: 

![](https://i.postimg.cc/nzMDydgY/2021-03-02-at-17-25-18.png)

# Conclusion
Choose **Fine-grained** access control when you are prompted to determine how to control access to objects during the **bucket** creation process unless the new bucket is created to contain all public accessible objects for sure. Choosing **Fine-grained** accessiblity level leaves you the leeway to switch back to Uniform level later, but not that easy the other way around if the bucket has already contained objects not publicly readable.

# References

|   |   |
|---|---|
|[1]|[Making data public](https://cloud.google.com/storage/docs/access-control/making-data-public)|



