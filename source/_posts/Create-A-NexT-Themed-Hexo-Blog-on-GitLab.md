---
title: Create A NexT-Themed Hexo Blog on GitLab
date: 2020-02-25 22:31:47
mathjax: false
tags: [Hexo, Git]
comments: true
---


To keep the tutorial concise, I proceed in a fashion that simply outlines the procedure without diving into what's under the hood. I hope this approach gives you a big picture of what you are going to accomplish and allow you to build up your Hexo blog site procedurally with a peaceful mind as I did. <!--more-->

<div class="image" 
     style="display:table; text-align:center; margin:30px auto;">   
  <img src="https://images.adsttc.com/media/images/5b44/f53b/f197/cc4b/e400/0287/slideshow/open-uri20180710-5710-oh5ha4.jpg?1531245875" 
       alt="" 
       style="width:400px; height:500px; border=0;"/>
  <div style="display:table-caption; caption-side:bottom;">   </div>
</div>



# Prerequisites

Download the latest version of (1) **Nodejs**, (2) **npm** and (3) **Git**. 

> Be sure they are up to date.

# Configuration

First, refer to the post "[Getting Started](https://theme-next.js.org/docs/getting-started/)" to install the framework of the static site generator **Hexo**. You will go through the process of (1) installing, (2) upgrading, (3) initiating, and (4) configuring of a popular Hexo theme, **NexT**, after establishing the backbone of Hexo.

>  It's noteworthy that do **NOT** directly modifying the `theme` config file. If you have no idea of what I am talking about, that's fine. We will reach the point again later.

Before moving onto the next step, you need to have:

1. A **GitLab** account. 

2. Initiate a new **project** on GitLab. 

   You can check out this GitLab document about "[creating a new page projects.](https://about.gitlab.com/blog/2016/04/07/gitlab-pages-setup/#creating-new-pages-projects)" Leave the **GitLab CI** setup to next paragraph. For general applications such as building your personal website, set the repository to **public**. 

3. Create a pair of SSH **keys** (public and private). 

   You can check out my post "Separate GitHub and GitLab SSH Keys in the Same Machine" to learn the creation of a SSH key for the access to GitLab. 

Second, refer to this post "[Deployment](https://theme-next.js.org/docs/getting-started/deployment.html)" (all the way to the [GitLab CI](https://theme-next.js.org/docs/getting-started/deployment.html#Gitlab-CI) section) to test your blog on local machines and, if things run as you expect, then deploy your blog to public web page services. After adding `.gitlab-ci.yml` to the root directory, you are almost there. What's left is to upload your blog to the GitLab repository using commands similar to what follows. 

But be mindful **NOT** to upload the directory which Hexo (or other static site generator) generated locally. Otherwise you'll have duplicated contents and you might face build errors. For example, do not commit the `public` directory in the Hexo deployment. (Based on my personal experience, this issue seems to be taken care of automatically by Hexo, so you don't know what I said. You should be fine.)

Here is an example of this final step:  

```shell
$ cd existing_folder
$ git init
$ ssh -T git@gitlab.com # check if the connect is built.
$ git remote add origin git@gitlab.com:libinruan/libinruan.gitlab.io.git
# Three key commands for Hexo deployment
$ git add .
$ git commit -m "Initial commit"
$ git push -u origin master
```

# Adding Theme Gadgets

Now, you are arriving at the milestone that you fully control what you Hexo website looks in public.

Firstly, run one of the following commands depending on how you installed the NexT theme. By doing so, you are ready to conduct alternative theme configuring as mentioned in the NexT official doc.

```shell
# If your NexT theme was installed through npm
cp node_modules/hexo-theme-next/_config.yml _config.next.yml
# If your NexT theme was installed through Git
cp theme/next/_config.yml _config.next.yml
```

Take this scenario for example.

If we like to set the theme scheme to `Gemini`, open `_config.next.yml` and copy the code snippet: 

```shell
theme_config:     # This is the section line.
  scheme: Gemini  # we make a 2-space indent to move key and value pairs rightward.
```

Paste the code snippet into your site's config file `_config.ymml`. Be sure to strictly follow the 2-space indentation rule when modifying either of the config files.


## Enabling Disqus Comment System

Check the official [doc](https://theme-next.js.org/docs/third-party-services/comments#Disqus) for details. The only bump on the road is not to mistake `shortname` for `website name`.

![](https://i.postimg.cc/c4wrp9TD/2021-02-27-at-16-04-17.png)

In the theme's config file, your setting should look similar to the following example:

![](https://i.postimg.cc/SKVNfHdP/2021-02-27-at-15-33-30.png)

## Rendering Math Equations
There exists many choices to render Latex math equation under Hexo. After a series of trials and errors, I decide to install **Hexo-Filter-MathJax** because, as of February 27, 2021, the approach widely recommended on the Internet that needs to invoke Hexo-Render-Pandoc doesn't work for my newly built Hexo site. Check its official GitHub [webpage](https://github.com/next-theme/hexo-filter-mathjax) for step-by-step installation instructions.

Sometimes, we may want to give our post the feature of numbering and referring multi-line equations rendered by MathJax. To do so, check NexT official [doc](https://theme-next.js.org/docs/third-party-services/math-equations#Numbering-and-Referring-Equations-in-MathJax) for a configuration instruction.

## Tracking View Counts

For tracking the view count of respective posts, my approach is to register a [leancloud](https://leancloud.app/) account, and then refer to the instruction of a Chinese tutorial, [为NexT主题添加文章阅读量统计功能](https://tinyurl.com/ycq2ncpg).

After the installation, if you run into the following error messages or glitches:

1. The `Counter not initialized! More info at console err msg.` error message displayed next to the view count icon,
2. The `TypeError: Cannot read property 'enable_sync' of undefined` error message when you run the `hexo clean && hexo g && hexo s` command, or
3. The value of view count is fixed at zero regardless of how many times you visited the post,

then you need to check out the official **README.md** file of the package in the directory for setting up your config file right:

```shell
[your-blog-archive]/node_modules/hexo-leancloud-counter-security
```

It's noteworthy that both the site's and the theme's config files need to be written up for enabling this feature.

Specifically, in your theme's cofig file add something similar to this template:

```shell
leancloud_visitors:
  enable: true
  app_id: HYu2e8NOepSkPGNaki6ORRwd-MdYXbMMI # <your app id>
  app_key: fFcNhMkx0fGED4yrEqsb8F14 # <your app key>
  betterPerformance: false
  security: false
```



In your site's config file, the configuration should look like something similar to this:

```shell
leancloud_counter_security:
  enable_sync: true
  app_id: HYu2e8NOepSkPGNaki6ORRwd-MdYXbMMI # <your app id>
  app_key: fFcNhMkx0fGED4yrEqsb8F14 # <your app key>
  # Required for apps from CN region
  server_url: # <your server url>
```



## Enabling the Creative Common Icon

In `/themes/next/_config.yml`, search for the block of `creative_commons`. Set the value of `post` to **true**.

## Changing Tag Icons in the Post Bottom

In `/themes/next/_config.yml`, set the value of `tag_icon` to **true**.



## Synchronizing Local Browser with Real-Time Blog Changes

Install the plugin `hexo-browsersync`.

## Enabling Google SEO

Check out this [link](https://theme-next.js.org/docs/theme-settings/seo) for details. The outline of the procedure is (1) select URL prefix, (2) choose the HTML Tag option, (3) copy and paste the `sitemap.xml` file to your site's **root directory**, and (4) get Google verify your setup. The only bump on the road is to copy and paste the generated verification code (**including** the double quotation marks around the combination) into your site's config file.

Next, stay in the Google Search Console, go to Help (the circle icon with a question mark), type and search for `robot.txt`, and get your robot.txt tested (if not exists, create one and save it in the folder `/source`) as mentioned in this [post](https://mutebardtison.github.io/2018/07/21/How-to-Get-Your-Hexo-Blog-Listed-Indexed-on-Google/).



# Creating a New Post

To create a new post template, run in the root directory:

```shell
$ hexo new post "filename-of-the-new-post"
```

It's worthy of noting that the **filename** of the new post needs not to be the same as the value of the field **title** you specify in the front-matter of the new post. Throughout your website, the identity of the new post is only displayed as its title rather than its filename. 







# Deployment

From now on the typical workflow for you to publish new Hexo blog posts to GitLab is as follows:

Step 1. Review your site locally before committing it to GitLab by running:

```shell
$ hexo clean && hexo g && hexo s --debug -p 4001 # 4000 by default
```

> IMPORTANT! Do not use the outdated approach `$ hexo d` to deploy your blog, if you choose **GitLab** as the remote repository.

Step 2. Once everything runs living up to your expectation, commit and push it to GitLab:

```shell
$ git add .
$ git commit -m "Some-Comment-On-Thie-Update"
$ git push -u origin master
```
You're all set.

# Troubleshooting

## Reverting to Previous Version

The GitLab CI pipeline would raise a red flag by sending you a failure message if your submission contains errors. To revert your work to previous normal edition, follow the procedure below:

1. Navigate to your blog's **commits** page on GitLab. For example: mine is located in 

   ```shell
   https://gitlab.com/libinruan/libinruan.gitlab.io/-/commits/master
   ```

2. Click on the **red cross** that is next to the latest version that causes the error. It looks similar to this screenshot:![](https://i.postimg.cc/W1K8zM9y/2021-02-27-at-10-23-50.png)

3. Choose **Revert** and proceed to **Merge**. 

4. After clicking on the button Merge, go back to local command line and run:

   ```shell
   $ cd /path/to/your/blog
   $ git pull
   ```

   Now, your blog are reverted back to the latest normal version. 

## Common Hexo Commands

Run `npm list` to check to see which package is listed as UNMET DEPENDENCY. Then `npm install` the package to resolve thie issue.

For instance:
```shell

$ ├── UNMET DEPENDENCY css-loader@^0.23.1
$ npm install css-loader@^0.23.1
```

# Conclusion

Now that you have a brand new blog running on GitLab, every time you post new articles or make some tweak on your blog, repeat the routine outlined above to preview, commit, and publish your blog. Happy blogging!

# References

|[1]|[Add your custom domain](https://about.gitlab.com/blog/2016/04/07/gitlab-pages-setup/#custom-domains)|
|---|---|
|[2]|[Hexo's GitLab pages](https://hexo.io/docs/gitlab-pages.html) for creating a project page other than a personal one |
|[3]|[NexT 實用的設定與功能](https://hsiangfeng.github.io/hexo/20190514/1899427194/)|
|[4]|[关闭Next主题菜单页面标题显示](https://tinyurl.com/ycm4zrmb)|
|[5]|[next主题中超链接颜色与样式的修改next主题中超链接颜色与样式的修改](https://www.taodudu.cc/news/show-2114461.html)|

[^footnote 1]: test

