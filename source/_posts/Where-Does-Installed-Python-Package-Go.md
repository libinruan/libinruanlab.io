---
title: (Todo) Where Does Installed Python Package Go?
date: 2019-02-27 21:38:44
tags: [pip]
categories: []
secret: true
---

The content of the current page remains under construction... 

# References

1. [你的 Python 包都装到哪了?](https://frostming.com/2019/03-13/where-do-your-packages-go/)
2. [Why did pip install a package into ~/.local/bin?](https://unix.stackexchange.com/questions/240037/why-did-pip-install-a-package-into-local-bin)
3. [What is the difference of the package under anaconda3/bin and anaconda3/lib/python3.7/?](https://stackoverflow.com/questions/58885461/what-is-the-difference-of-the-package-under-anaconda3-bin-and-anaconda3-lib-p)

