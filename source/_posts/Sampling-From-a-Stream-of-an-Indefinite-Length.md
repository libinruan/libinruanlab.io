---
title: Sampling From a Stream of an Indefinite Length
tags: []
categories: []
secret: false
comments: true
date: 2020-06-05 16:31:41
sticky:
---

How do we ensure that the probability of an item in a stream being selected is no different than those of the others? Reservoir Sampling is the algorithm that comes to our rescue. In this post I lazily showed why Reservoir Sampling works.

<!--more-->

![](https://i.postimg.cc/66yFPscT/2021-06-13-at-16-32-57.png)

