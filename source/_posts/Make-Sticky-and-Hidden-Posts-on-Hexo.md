---
title: Make Sticky and Hidden Posts on Hexo
date: 2020-02-27 21:22:20
tags: [Hexo]
categories: []
secret: true
---

# Sticky Notes

Did you ever wonder about the way to pin your posts on top of the index page? Check this NexT theme's official [doc](https://theme-next.js.org/docs/advanced-settings/front-matter.html?highlight=sticky+pin#Settings-amp-Their-Default-Values) for making **sticky posts** on a Hexo blog. 

---

# Hidden Posts

Besides, if you want to extend the blog to be an archive to collect your private articles, install this [package](https://github.com/printempw/hexo-hide-posts). As mentioned in the package readme file, you need to manually insert the following parameters into the site's config file:

```shell
# hexo-hide-posts
hide_posts:
  # Name the field in the front-matter block as you prefer
  filter: hidden # I name the field "secret" instead
  # Generators which you want to expose all posts (include hidden ones) to.
  # Common generators: index, tag, category, archive, sitemap, feed, etc.
  public_generators: [tag, category, archive, sitemap] # In this case, I still want my private posts to be shown in every place except for the home page of the blog.
  # Add "noindex" meta tag to prevent hidden posts from being indexed by search engines
  noindex: true
```

If you want to have control beyond what the package brings, check this post "[hexo+next下隐藏文章与其导航|hexo+next-hidden-post](https://www.jensenhong.com/2020/10/02/hexo-next-hidden-post/#more)." It shows how to create a **private navigation page** for you to much more easier browse your private posts.



