---
title: Kaggle101 Reveiw of Winnder Models for Tabular Data (1)
secret: true
tags: [tabular, classifier, regressor]
---
# behavior questions
Video 1. [Q: difficult tasks.](https://www.youtube.com/watch?v=ZLtO_7LjzVg&t=1m42s). [sample answer 1](https://www.youtube.com/watch?v=ZLtO_7LjzVg&t=3m8s). [Q: stressful situation](https://www.youtube.com/watch?v=ZLtO_7LjzVg&t=5m25s). [Q: embrace changes in organization](https://www.youtube.com/watch?v=ZLtO_7LjzVg&t=7m39s).[Q: demonstrate flexibility](https://www.youtube.com/watch?v=ZLtO_7LjzVg&t=8m56s). [Q: biggest failure](https://www.youtube.com/watch?v=ZLtO_7LjzVg&t=10m14s).[Q: how to deal with people not pulling their weight at work](https://www.youtube.com/watch?v=ZLtO_7LjzVg&t=11m17s).[Q: handle harassment or bullying](https://www.youtube.com/watch?v=ZLtO_7LjzVg&t=12m39s).
Video 2. [Q: do you have any questions?](https://www.youtube.com/watch?v=EbA5rzpSWLM&t=38s). At least 3 questions (one that is success dirven; one that is culture related; one that is more connecting; one asking for feedback) in your pocket. More than "What's the next step of the process?" Ask cultural questions like "What's a typical work week like here?" (don't ask what's a typical day.) "What's it you would like someone in this position to accomplish in the first 90 days? / What's the one trait that you see in people that make really successfully in this position? / how is the past employee successful in this position? / What are some of the challenges you foresee in the next six months someone in this position would be working on? / (connect with the interviewer more and also get to learn more about the culture) What's your favorite part for you working in this department? (Not the company culture. Don't ask questions you can google the answer to. Specify the department culture). **Based on what we've talked about today is there anything that is leaving you with hesitancy with hiring me for this position** ([timestamp](https://www.youtube.com/watch?v=EbA5rzpSWLM&t=5m58s)). How to argue when the feedback is not positive? ([Answer](https://www.youtube.com/watch?v=EbA5rzpSWLM&t=7m)) - That's why I'm so excited to work here. I love a new challenge and I'm excited to learn something new and I know I'll just hit the ground running because learning something new and different doing something I haven't done before **keeps me going** and so I'm excited to learn more about that area to work in it. / (or the feedback is that I don't like yoru blah blah..) Oh I'm sorry could I give you another story ... **Make use of the last chance to fight for the position, to clarify uncertainty.** ([timestamp](https://www.youtube.com/watch?v=EbA5rzpSWLM&t=7m55s)) 

# algorithm
![](https://i.postimg.cc/Y9gvgny7/2021-03-21-at-10-40-25.png)
[九張算法班](https://www.jiuzhang.com/course/71/?utm_source=sc-v2ex-swj0318)

# PyTorch quickstart guide

- [Pytorch Tutorial for Deep Learning Lovers](https://www.kaggle.com/kanncaa1/pytorch-tutorial-for-deep-learning-lovers) is a kaggle notebook that contains short exercises to get you ready.
  - According to [PyTorch developers](https://discuss.pytorch.org/t/problems-with-target-arrays-of-int-int32-types-in-loss-functions/140/3?u=jdhao), some use cases requires that the target be LongTensor type and int just can not hold the target value.
- [Understanding Pytorch hooks](https://www.kaggle.com/sironghuang/understanding-pytorch-hooks) explains the function of hooks in a Kernal notebook.
  - Pytorch **hook** records the specific error of a parameters (weights, activations, etc) at a specific training time. One of the applications of hooks is to visualize neural network based on these gradient records ([GRAD-CAM](https://arxiv.org/pdf/1610.02391.pdf)).
  - [In PyTorch, what is the difference between forward() and an ordinary method?](https://stackoverflow.com/questions/58508190/in-pytorch-what-is-the-difference-between-forward-and-an-ordinary-method) 
  - Review:
    - Back-propagations. 
      - [Back-Propagation is very simple. Who made it Complicated ?](https://medium.com/@14prakash/back-propagation-is-very-simple-who-made-it-complicated-97b794c97e5c)
      - [A Step by Step Backpropagation Example](https://mattmazur.com/2015/03/17/a-step-by-step-backpropagation-example/)
        - A auxiliary kaggle notebook that replicates the numeircal example in the post can be found at [Understanding Pytorch hooks](https://www.kaggle.com/sironghuang/understanding-pytorch-hooks).
  - Takeaway: Do NOT call the forward(x) method. You should call the whole model itself, as in model(x) to perform a forward pass and output predictions. If you call the .forward() method, and have hooks in your model, the hooks won’t have any effect.
- [Argument *requires_grad*](https://pytorch.org/docs/stable/notes/autograd.html)

# References

| [1]  |  [1st place solution: text version](https://www.kaggle.com/c/lish-moa/discussion/201510)  |
| ---- | ---- |
| [2]  |  [1st place solution: github repository](https://github.com/guitarmind/kaggle_moa_winner_hungry_for_gold)  |
| [3]  |  [2nd place solution](https://www.kaggle.com/c/lish-moa/discussion/202256)  |
| [4]  |  [3rd place solution](https://www.kaggle.com/c/lish-moa/discussion/202078)  |
| [5]  |  [4th place solution](https://www.kaggle.com/c/lish-moa/discussion/200808)  |

# experiments

1. min frequency = 0.01 ![](https://i.postimg.cc/nhvkCPww/2021-03-16-at-11-32-06.png)
2. min frequency = 0.005 ![](https://i.postimg.cc/MpwNGVpx/2021-03-16-at-11-23-55.png)
3. min frequency = 0.001 ![](https://i.postimg.cc/nzSQw4CJ/2021-03-16-at-11-20-25.png)
4. min frequency = 0.0005 ![](https://i.postimg.cc/HLRrwpKY/2021-03-16-at-11-21-58.png)
5. parameter experiment ![](https://i.postimg.cc/g2sBbJHH/2021-03-17-at-12-08-08.png)

# Mechanisms of Action (MoA) Prediction

[Overview](https://www.kaggle.com/c/lish-moa/discussion/201510)

**Mechanism of Action**. In medicine, a term used to describe how a drug or other substance produces an effect in the body. For example, a drug's mechanism of action could be how it affects a specific target in a cell, such as an enzyme, or a cell function, such as cell growth. 

**Data**. The data is based on a new technology that measures simultaneously (within the same samples) human cells’ responses to drugs in a pool of 100 different cell types. You will have access to MoA annotations for more than 5,000 drugs in this dataset.

**Task**. Develop an algorithm that automatically labels each case in the test set as one or more MoA classes. Note that since drugs can have multiple MoA annotations, the task is formally a **multi-label classification** problem.

**Evaluation**. Based on the MoA annotations, the accuracy of solutions will be evaluated on the average value of the [logarithmic loss](https://www.kaggle.com/c/lish-moa/overview/evaluation) function applied to each drug-MoA annotation pair..

# 1st place solution

## Overview

**Architecture**. [1st place solution](https://www.kaggle.com/c/lish-moa/discussion/201510) is a blender composed of 7 single models ranging from simple NN, multi-stage NN with TabNet, 2-heads ResNet to EfficientNet with Noisy Student.

![](https://www.googleapis.com/download/storage/v1/b/kaggle-forum-message-attachments/o/inbox%2F201774%2F0a0b63df266de28b20d55046de9776da%2Ffigure_1.png?generation=1607159993332157&alt=media)

**Blending weights**. The selection of blend weights were based on two factors: the LB scores and correlations between the single models.  Models with less mean correlations were given higher weights. 

**Single model selection**. The selection of single models in our Best CV blend is fully based on the OOF (Out-of-folds) predictions. We used the TPE (Tree-structured Parzen Estimator) sampler in [Optuna](https://optuna.readthedocs.io/en/stable/reference/generated/optuna.samplers.TPESampler.html#optuna.samplers.TPESampler) and SLSQP (Sequential Least Squares Programming) method in Scipy for searching CV-optimized weights. 

The addition of [DeepInsight](https://alok-ai-lab.github.io/DeepInsight/) CNNs played a significant role in our final blends because of their high diversity to other shallow NN models.

**Cross-vaidation strategy**. Most of our models are based on the **[MultilabelStratifiedKFold](https://github.com/trent-b/iterative-stratification)** (old CV), with the exception of a Simple NN model that used the new **[double stratified CV](https://www.kaggle.com/c/lish-moa/discussion/195195)** shared by @cdeotte using drug_id information. We used different seeds for the CV splits in our models. The choice of CV strategy is based on the goodness of alignment to the CV log loss and LB log loss. The K of CV is either 5 or 10. We also trained the models using multiple seeds to reduce the variance.

Notably, we noticed that it is difficult to combine the OOF files from both old and new CVs for searching optimized CV weights as the log loss scores of new CV models are much higher. We ended up selecting only old CV models for our Best CV blend, which scored 0.15107 on old CV and 0.01601 on private LB.

**Overfitting resolution**. To fight with the risk of overfitting under this small and highly imbalanced multi-label dataset, we imposed **label smoothing** and **weight decay** as the regularization methods in the model training process.

**Workflow**. 
- **Training All Models Respectively**. In the first (training) stage, training all single models to generates pickled preprocessing modules, model weights used by inference scripts, and 
  - Execute `sh train.sh <input folder> <model folder>` to 
  - All models will be saved under the final folder with fixed folder names.
- **Run Inference on All Single Models**. run inference on all single models to make predictions used in blend weights search. 
  - `sh inference.sh <input folder> <model folder> <output folder>`
  - All model predictions will be saved under the target output folder with fixed submission filenames.
- **Blending Model Inference Outputs**. The search tries to determine good blending weights for the ensemble of models (Author: The weights in the submission notebooks need to be updated manually!?). 
  - `python blend.py <input folder> <prediction file folder> <output folder>`

In the inference stage, the goal is to make inference by running each single-model inference scripts and blending the predictions. All of the training notebooks must be added as dataset to load preprocessing class instances and model weights. 

## Model 1. 3-stage-nn

![](https://i.postimg.cc/mgM6QpJ9/inbox-201774-fb1fcba2f3331b3d3ac90cda457c23bd-figure-2.png)

Ensure that you're using Python 3 rather than Python 2.

To run the training shell script, `<input folder>` could be for example `/kaggle/input` and `<model folder>` could be `/kaggle/models`. The full command is like:
```shell
!python3 train.sh /kaggle/input /kaggle/models
```

All necessary ouputs from a preproscessing model are stored in MODEL_DIR = output/kaggle/working/model. Put those into dataset, and load it from inference notebook

The author's workflow seems to work on notebooks during competition and pack files into .py and .sh during publication.

For running a notebook the author provided, copy the notebook to the directory `/kaggle/input`.

Quantile Transformer (scikit-learn [doc](https://scikit-learn.org/stable/modules/generated/sklearn.preprocessing.QuantileTransformer.html)) to spread out the most frequent values and also reduces the impact of (marginal) outliers.

Custom dataset in Pytorch (check [data loading tutorial](https://pytorch.org/tutorials/beginner/data_loading_tutorial.html#dataset-class))

Why do we divide running loss by len(dataset)? -[link](https://discuss.pytorch.org/t/dont-understand-loss-calculation/31327/3)

Does model.train() and model.eval() change any behavior of the gradient calculations? NO. They are used to set specific layers like dropout and batchnorm to **evaluation** mode (dropout won't drop activations, batchnorm will use running estimates instead of batch statistics) -[source](https://discuss.pytorch.org/t/model-eval-vs-with-torch-no-grad/19615/37?u=libinruan)

log loss ([scikit](https://scikit-learn.org/stable/modules/generated/sklearn.metrics.log_loss.html)-learn)

Why uses `iterative-stratification`? [Answer](https://pypi.org/project/iterative-stratification/). And how to install it on Kaggle without the Internet. -[source](https://www.kaggle.com/c/lish-moa/discussion/190128)


## BNP Paribas Cardif Claims Management

'1' [1st place solution](https://www.kaggle.com/c/bnp-paribas-cardif-claims-management/discussion/20247). 
'2' [2nd place solution](https://www.kaggle.com/c/bnp-paribas-cardif-claims-management/discussion/20252). 
'3' [3rd place solution](https://www.kaggle.com/c/bnp-paribas-cardif-claims-management/discussion/20258).
'4' [特征编码总结 Kaggle](https://zhuanlan.zhihu.com/p/117230627) interview 可用，非常好，值得一讀再讀. From "4", One-Hot Encoding is applicable on cases where the number of categories is small (i.e. use less memory) and algorithms are sensitive to the magnitude of values (e.g., SVM and LR). Oftentime OHE is suitable for cases where therer are less than 5 categories （我覺得 nn 不在此限）. [kaggle编码categorical feature总结](https://zhuanlan.zhihu.com/p/40231966) 也很有特點，面試重要。![](http://postimg.cc/MXVQXyz8)
'5' [机器学习之类别特征处理](https://www.biaodianfu.com/categorical-feature.html) interview 可用，非常好，值得一讀再讀. ![One-hot encoding 優缺點](https://i.postimg.cc/Bn5G7DDC/2021-03-11-at-17-06-53.png), ![高基數定性特徵不能用LE and OHE](https://i.postimg.cc/9fGt7m60/2021-03-11-at-17-14-12.png) ![目標編碼](https://i.postimg.cc/gJ1SWz6K/2021-03-11-at-18-13-47.png). [Additive smoothing](https://www.kaggle.com/dustinthewind/making-sense-of-mean-encoding#894754).
'6' [category_encoders](https://github.com/scikit-learn-contrib/category_encoders).
'7' [LR, Lasso regression, Ridge regression數學解與其自寫python碼](https://zhuanlan.zhihu.com/p/158788222) interview 可用, 
'8' [SVD, Orthogonal matrix, Covariance matrix的關聯與LR上的應用](https://zhuanlan.zhihu.com/p/132275334) interview 可用, "正则化的目的在于让预测值对于单一自变量X的敏感程度降低，具体降低的程度取决于我们的正则化力度"
'9' [回归的多种写法：线性回归-贝叶斯线性回归-高斯过程回归（理论篇）](https://zhuanlan.zhihu.com/p/350389546) 備用,
'10' [正则化是什么？以及Ridge和 Lasso回归的区别](https://zhuanlan.zhihu.com/p/111562068) "正则化的目的在于让预测值对于单一自变量X的敏感程度降低，具体降低的程度取决于我们的正则化力度".
'11' [奇異矩陣、共線性問題的數學解釋與正則化解決方案](https://zhuanlan.zhihu.com/p/30535220) interview, "权衡偏差(Bias)和方差(Variance)的问题。方差针对的是模型之间的差异，即不同的训练数据得到模型的区别越大说明模型的方差越大。而偏差指的是**模型预**测值与**样本数据**之间的差异。"
'12' [Standardization，Normalization和Regularization的区别](https://zhuanlan.zhihu.com/p/95726841) interview.
'13' [Imputing missing values with variants of IterativeImputer](https://tinyurl.com/ygno3muu) contains Bayesian Ridge.
'14 [sklearn.linear_model.RidgeClassifierCV](https://tinyurl.com/ydtt9mdy) contains details on hyperparameters of ridge regression. Good for interview.
'15' [StratifiedKFold and StratifiedShuffleSplit in sklearn](https://stackoverflow.com/questions/45969390/difference-between-stratifiedkfold-and-stratifiedshufflesplit-in-sklearn)
'16' [Target Encoding Vs. One-hot Encoding with Simple Examples](https://tinyurl.com/yg4kqwku) contains works on OHE+PCA.
'17' [K-Fold Target Encoding](https://medium.com/@pouryaayria/k-fold-target-encoding-dfe9a594874b) contains nice summary and a nice custom code/**class**. Short post for interview. ![](https://miro.medium.com/max/875/1*ZKD4eZXzd_FdN0SQDszFVQ.png). Also check 『26』 for a scikitlearn pipeline integrated with logistic regression. And the API doc of Category Encoders for [Target Encoder](https://contrib.scikit-learn.org/category_encoders/targetencoder.html). And check this [Kaggle kernel](https://www.kaggle.com/dustinthewind/making-sense-of-mean-encoding#894754) that provides a non-class version of target encoder implementation.
'18' [Slice retrieval, slice assignment and chained assignment](https://stackoverflow.com/questions/54932459/when-the-slice-do-shallow-copy-and-when-it-do-deep-copy-in-python-3)
'19' [Select dtypes, percentile group在pandas中的应用](https://zhuanlan.zhihu.com/p/94382243)
'20' [Python中的copy.copy()和copy.deepcopy()区别在哪里?:酒量小的桃子](https://www.zhihu.com/question/326220443#Popover34-toggle)
'21' 我对the impact of "slicing is a shallow copy"的理解。先看这以下例子：
```python
a = [1,2,3]
b = a[:] # shallow copy, id(b)!=id(a)但是目前內部references到相同的物件1,2,3
b[2] = 0 # 由於a[3]是不可變對象（整數），則按照法則，b[2]會綁到新的變量上
print(a) # [1,2,3]
print(b) # [1,2,0]
```
還有這個
```python
a = [[1],[2],[3]]
b = a[:] # 同上
b[2][0] = 0 # b[2]綁定的對象是可變對象(list)，修改b[2]的id值保持不變，但是其內容物發生變化。由於b[2]與a[2]綁定同一個對象，很顯然地，對b[2]的修改會映射到a[2]上。
print(a) # [[1], [2], [0]]
print(b) # [[1], [2], [0]]
```
以上推論參考『22, 23』
'22' [Slice assignment (when you have a slice on the left side of `=`) modifies an existing list; slicing creates a new list (a shallow copy, that is, with a different ID but element references are the same)](https://tinyurl.com/yhotpedq)
'23' [Does a slicing operation give me a deep or shallow copy?](https://stackoverflow.com/questions/19068707/does-a-slicing-operation-give-me-a-deep-or-shallow-copy)
'24' [Python's list slicing and slice assignment](https://www.30secondsofcode.org/blog/s/python-slice-assignment) quick start guide with code examples. Excellent.
'25' [Views and copies in Pandas](https://www.practicaldatascience.org/html/views_and_copies_in_pandas.html)
'26' [Target encoding with KFold cross-validation - how to transform test set?](https://datascience.stackexchange.com/questions/81260/target-encoding-with-kfold-cross-validation-how-to-transform-test-set) provides a nice pipeline integrated with logistic regression.
'27' [What is the difference between pipeline and make_pipeline in scikit?](https://stackoverflow.com/questions/40708077/what-is-the-difference-between-pipeline-and-make-pipeline-in-scikit#answer-40708448)
'28' 有關ColumnTransformer的實驗
```python

import pandas as pd
data = pd.DataFrame([
    ('a',1,1), 
    ('a',2,0),
    ('b',3,0),
    ('c',4,1)],
    index = ['a1','a2','a3','a4'], columns = ['cat','num','tar']
)
from sklearn.compose import ColumnTransformer
from sklearn.preprocessing import MinMaxScaler, OneHotEncoder

num_tr1 = ColumnTransformer(transformers=[
    ('ss', MinMaxScaler(), ['num']),
], remainder='passthrough')
num_tr1.fit_transform(data)

num_tr2 = ColumnTransformer(transformers=[
    ('ss', MinMaxScaler(), ['num']),
], remainder='drop') # default
num_tr2.fit_transform(data)

cat_tr1 = ColumnTransformer(transformers=[
    ('oh', OneHotEncoder(), ['cat'])
], remainder='drop') # default
cat_tr1.fit_transform(data)

cat_tr2 = ColumnTransformer(transformers=[
    ('oh', OneHotEncoder(), ['cat'])
], remainder='passthrough') 
cat_tr2.fit_transform(data)

# 誰先誰後結果都一樣
# 想像the initial resulting object is an emtpy data frame.
# 想像the full data set 會一個都不缺地傳給每一個transformer
# 想像所得到的結果都會依序填入那個dataframe。
poo_tr1 = ColumnTransformer(transformers=[
    ('oh', OneHotEncoder(), ['cat']),
    ('ss', MinMaxScaler(), ['num']),
])
poo_tr1.fit_transform(data)

# 有沒有remainder=passthrough，決定完全沒被叫到的去留
poo_tr1 = ColumnTransformer(transformers=[
    ('oh', OneHotEncoder(), ['cat']),
    ('ss', MinMaxScaler(), ['num']),
], remainder='passthrough')
poo_tr1.fit_transform(data)
```
'29' [GridSearchCV從三個候選者尋找最佳Scaler()的例子](https://stackoverflow.com/questions/63698484/how-to-test-preprocessing-combinations-in-nested-pipeline-using-gridsearchcv) 
'30' [如何寫自製的function成為transformer：逐行擠出新的數據](https://scikit-learn.org/stable/auto_examples/compose/plot_column_transformer.html#sphx-glr-auto-examples-compose-plot-column-transformer-py)
'31' [如何應付ONE-HOT應用時，訓練數據跟測試數據的CATEGORICAL缺漏不對應的問題](https://towardsdatascience.com/using-columntransformer-to-combine-data-processing-steps-af383f7d5260)
'32' [Error message: fit_transform() takes 2 positional arguments but 3 were given with LabelBinarizer](https://stackoverflow.com/questions/46162855/fit-transform-takes-2-positional-arguments-but-3-were-given-with-labelbinarize).
'33' [Dropping one of the columns when using one-hot encoding ONLY for linear models without regularization, not applicable on tree based models and regularized linear models](https://stats.stackexchange.com/questions/231285/dropping-one-of-the-columns-when-using-one-hot-encoding)
'34' [Automate Publishing of Jupyter Notebooks as Medium Blog Posts with jupyter_to_medium](https://medium.com/dunder-data/jupyter-to-medium-initial-post-ecd140d339f0)
For Applications of Custom Column Transformer, Check **'35', '36', '37' and '38', '47'** below. They covered what a developer needs.
'35' [Building a Scikit-Learn ColumnTransformer Dynamically](https://www.kaggle.com/kylegilde/building-columntransformers-dynamically) provides an excellent template to distinguish high-cardinality categorical features from low-cardinality ones and then pass them separately into two different transformers for preprocessing.**Excellent**!!
'36' [Using ColumnTransformer to combine data processing steps](https://towardsdatascience.com/using-columntransformer-to-combine-data-processing-steps-af383f7d5260) provides an excellent quick start guide, especially Tip 3 and 4 for dealing with 'rare' categorical features or flags and the pipeline advice on trasforming columns one at a time. Some useful quotes, "Note that ColumnTransformer “sends” the columns as a numpy array. To convert these timestamps from strings, I cast them as a pandas DataFrame," and "If we have any rare categorical features that end up not present in each of these groups, the default OneHotEncoding settings are going to produce different numbers of columns for the different input sets."
'37' **Excellent**! 如何用**pipeline**而不是columnTransofrmer自動選columns [A comprehesnive demsontration: Scikit-learn Pipelines: Custom Transformers and Pandas integration](https://queirozf.com/entries/scikit-learn-pipelines-custom-pipelines-and-pandas-integration) **非常好** and this stackoverflow post about [How to create a custom Python class (transformers) to be used in Pipeline for dropping highly correlated features?](https://stackoverflow.com/questions/66221834/.how-to-create-a-custom-python-class-to-be-used-in-pipeline-for-dropping-highly-c) creates transformers to drop highly correlated features and integrate `GridsearchCV` with `Keras`. 此篇顯示如何將Keras引入GridsearchCV。用Optuna似乎更簡單。
'38' [如何結合pipeline+gridsearch with keras model for optimization](https://stackoverflow.com/questions/66221834/how-to-create-a-custom-python-class-to-be-used-in-pipeline-for-dropping-highly-c) and [高階玩法:Selecting dimensionality reduction with Pipeline and GridSearchCV](https://scikit-learn.org/stable/auto_examples/compose/plot_compare_reduction.html#sphx-glr-auto-examples-compose-plot-compare-reduction-py) contains the guide on searching over a set of imputer transformers. The use of 'passthrough'. **Nice**!!
'39' [Scikit-learn cross validation API doc](https://scikit-learn.org/stable/modules/cross_validation.html)
'40' [Optuna+KFold+Ridge Regression: quick start guide](https://towardsdatascience.com/tuning-hyperparameters-with-optuna-af342facc549) Nice!
'41' [Encode Smarter: How to Easily Integrate Categorical Encoding into Your Machine Learning Pipeline](https://innovation.alteryx.com/encode-smarter/) ![](https://innovation.alteryx.com/content/images/2019/08/categorical-encoding-01-01.png)
'42' [Should I scale it after one-hot encoding a categorical features?](https://tinyurl.com/yffba6l6) says "With unpenalized linear models, there is no difference. The coefficients will just scale to counteract the new scale of the variables, and the intercept will shift to compensate for the centering. With penalized linear models though, there will be a difference. Since the standard deviation of a binary variable is at most 1/2, you'll be increasing the overall scale of the variable by standardizing. That will cause the unpenalized coefficient to decrease in magnitude, which will change the balance of how the penalty applies to different features. I suspect there is no "better" approach then: sometimes the penalty improves performance when the dummies are scaled, and sometimes degrades performance." 有標準化的必要，實驗看看才知道，要因地制宜。
'43' [why should we use smoothing for target encoding?](https://maxhalford.github.io/blog/target-encoding/)
'44' [why we use BaseEstimator in sklearn.base (python)](https://stackoverflow.com/questions/15233632/baseestimator-in-sklearn-base-python#answer-15242142) says "BaseEstimator provides among other things a default implementation for the get_params and set_params methods, see [the source code]. This is useful to make the model grid search-able with GridSearchCV for automated parameters tuning and behave well with others when combined in a Pipeline." IT's the central piece of transformer, regressor, and classifier. All estimators in scikit-learn are derived from this class. In more details, this base class `enables to set and get parameters` of the estimator. [why we use TransformerMixin in transformer](https://sklearn-template.readthedocs.io/en/latest/user_guide.html) says "In addition, scikit-learn provides a mixin, i.e. sklearn.base.TransformerMixin, which implement the combination of fit and transform called fit_transform." Check the official user guide for examples of transformer and predictor(regressor and classifier).
'45' [ValueError: n_splits=5 cannot be greater than the number of members in each class.](https://tinyurl.com/yg9ok9kz) shortly answers how stratified CV works in plain english.
'46' [When should I use `shuffle` in KFold?](https://tinyurl.com/yfowvd5s) says "The "shuffle" is mainly useful if your data is somehow sorted by classes, because then each fold might contain only samples from one class (in particular for `stochastic gradient decent classifiers` sorted classes are dangerous)." and "If your parameters depend on the shuffling, this means your parameter selection is very unstable. Probably you have very little training data or you use to little folds (like 2 or 3)." 參數不穩定有可能是cv切割數太小了。
'47' [Transformer for "column grouper" transformers](https://stackoverflow.com/questions/48320396/create-a-custom-sklearn-transformermixin-that-transforms-categorical-variables-c) Very nice!! 非常好
'48' [Pandas sum vs len](https://stackoverflow.com/questions/62143848/pandas-mean-vs-sum-len) states that `sum()` skips NaN by default while `len()` counts NaN.
'49' 生新的維度，可以用`[:,np.newaxis]` and `reshape(-1,1)`.
'50' [Does correlated input data lead to overfitting with neural networks?](https://stats.stackexchange.com/questions/232534/does-correlated-input-data-lead-to-overfitting-with-neural-networks) says "Correlated data means you should work harder to make the handling of data technically simpler and more effective. Overfitting can occur, but in won't happen because there is correlated data."
'51' [How to get and set parameters in transformers?](https://ig248.gitlab.io/post/2018-11-21-transformer-factory/) demonstrates the reason of using Pipeline rather than FunctionTransformer. Nice! Most imortant of all, it contains a quick start guide for `set` and `get` paramters of transformers.
'52' 面試草稿
- 1. run fillna method. (scikit-learn's imputer only works with np.nan). (Be careful missing value themselves may bear preditvtive value. Think twice about the way for filling np.nan's or imputing them).
- 2. Use add_indicator functionality calls the MissingIndicator class to create binary missing-indicator columns for features with missing values. In cases where data are not missing at random, this step helps improve performance.
- 3. use imputers (use median instead of mean guards against the influence of outliers); more mature, KNNImputer, which requires centering and scaling or the InterativeImputer.
- 4. (optional) liear model needs a more step: scaling
- 5. Grouping low frequency 
- 6. Categorical wit moderate-to-low cardinality: One-hot encoding.
- 7. Categorical features with high cardinality: likelihood encoding/target encoding.
'53' [Introduction to linear mixed models](https://stats.idre.ucla.edu/other/mult-pkg/introduction-to-linear-mixed-models/) Nice for interview. ![](https://i.postimg.cc/g2yBhdmC/2021-03-15-at-23-55-56.png) It describes the method as "This is really the same as in linear regression, where we assume the data are random variables, but the parameters are fixed effects. Now the data are random variables, and the parameters are random variables (at one level), but fixed at the highest level (for example, we still assume some overall population mean)."
'54' [Nice summary on data clearning from IBM](https://www.ibm.com/garage/method/practices/reason/prepare-data-for-machine-learning/)
'55' [Why do we need to Box-Cox transform variables in statstical modeling (regression or a design of experiments) 似乎也能回答『要不要scaling after one-hot encoding』的問題.](https://blog.minitab.com/en/applying-statistics-in-quality-projects/how-could-you-benefit-from-a-box-cox-transformation) says this asymmetrical behavior may lead to a bias in the model. ![](https://i.postimg.cc/V6WKSHGF/2021-03-16-at-11-18-00.png) And check this post: [How to Use Power Transforms for Machine Learning](https://machinelearningmastery.com/power-transforms-with-scikit-learn/).
'56' [Credit Models and Binning Variables are Winning and I’m Keeping Score!](https://odsc.com/blog/credit-models-and-binning-variables-are-winning-and-im-keeping-score-blog/) talsk why classification (**scorecard** - credit scoring to determine credit risk---the likelihoood a particular loan will be paid back, typically a logistic regression **model**) is widely used in the banking industry. 可以作為口頭報告的參考。It mentions "One of the best parts of binning is that **outliers** get lumped into the first or last bin and missing values can even have their own bin. This makes modeling much easier." 所以不要直接丟棄任何數據，要給怪異的數據分類。
'57' [This may speed up the tree-based boosting algorithm though for sure](https://stats.stackexchange.com/questions/68834/what-is-the-benefit-of-breaking-up-a-continuous-predictor-variable#answer-68839) Nice for diving into it. Good for interview. Benefit one: mitigate the effects of significant outliers that skew coefficients. Benefit two: Sometimes a distribution naturally lends itself to a set of classes, in which case dichotomization will actually give you a higher degree of accuracy than a continuous function.
'58' [What is the intuition behind SVD?](https://stats.stackexchange.com/questions/177102/what-is-the-intuition-behind-svd) 
'59' [In linear regression, there is no assumption of the distribution of independent variables]() BUT skewness may have impact on linera regression models. Just see this two nice Quora posts say ["Disproportionate influence on parameter estimates by skewness"](https://www.quora.com/How-does-skewness-impact-regression-model) and [a nice bernoulli random variable example](https://www.quora.com/What-are-the-consequences-of-a-skewed-predictor-independent-variable-in-a-linear-logistic-regression-model).
'60' [Skew() function in Pandas and its meanings](https://medium.com/@atanudan/kurtosis-skew-function-in-pandas-aa63d72e20de) and [How to use Box Cox transformation to deal with skewness in scipy](https://brianmusisi.com/design/Predicting+House+Prices-2.html) and [use Box Cox in Scikit-learn](https://machinelearningmastery.com/discretization-transforms-for-machine-learning/). I think it contains plentful information for data preprocessing about changing data distribution in linear regression modeling. Good for interview. Also you can check the [wiki](https://en.wikipedia.org/wiki/Data_binning) about data binning.
'61' [If your targets are distributed bimodally, binning it doesn't help (even make the performance worse).](https://web.ma.utexas.edu/users/mks/statmistakes/dividingcontinuousintocategories.html).
'62' [Why do we use ElasticNet? Is it better than Ridge or Lasso](https://www.quora.com/Is-ElasticNet-always-better-than-Ridge-or-Lasso) Nice for interview.
'63' [How does Sklearn's Power Transformer work in the fit and transform phases?](https://medium.com/@patricklcavins/using-scipys-powertransformer-3e2b792fd712)
'64' [Do we need to standardize one-hot encoded features?](https://datascience.stackexchange.com/questions/80234/possible-harm-in-standardizing-one-hot-encoded-features) has a nice short answer.
'65' [Comparison and theory of categorical encoding: Target encoder vs Bayesian encoder](https://towardsdatascience.com/target-encoding-and-bayesian-target-encoding-5c6a6c58ae8c) **Nice** for interview.
'70' [Hierarchical Bayesian Target Encoding](https://www.kaggle.com/mmotoki/hierarchical-bayesian-target-encoding) with nice custom transformers. Excellent transformer quick start guide. 連續變數的target encoder，好。
'71' [detailed basic pandas review: kaggle kernel on Titanic data](https://www.kaggle.com/dixhom/bayesian-optimization-with-optuna-stacking) includes Optuna and Stacking.
'72' [Repeated k-Fold Cross-Validation](https://machinelearningmastery.com/repeated-k-fold-cross-validation-with-python/) mentions "repeated k-fold cross-validation replicates the procedure multiple times. For example, if 10-fold cross-validation was repeated five times, 50 different held-out sets would be used to estimate model efficacy."
'73' [Hierarchical Bayesian Target Encoding](https://www.kaggle.com/mmotoki/hierarchical-bayesian-target-encoding) Excellent!! Math and simple code for `regressors`. Interview review. References from MIT: [class 12](https://math.mit.edu/~dav/05.dir/class12-slides-all.pdf) 課程12到15都值得看。面試機率複習。Class 14 is related to **binary classification** and Class 15 is related to **regression**. Pretty concise!
'74' [Beta Target Encoding](https://mattmotoki.github.io/beta-target-encoding.html) continues the same discussion of bayesian target encoding and gives more mathematical treatment about the topic. It includes reference for the code to implement bayesian target encoding for `binary classification`. And it also clearly explain why a generic LightGBM or CatGGM's built-in Target Encoding is not good. **Excellent** post! For interview. [Code](https://www.kaggle.com/mmotoki/avito-target-encoding) can be found here.
'75' [Hands-On Python Guide to Optuna – A New Hyperparameter Optimization Tool](https://analyticsindiamag.com/hands-on-python-guide-to-optuna-a-new-hyperparameter-optimization-tool/) is an excellent quick start guide with demonstartion on advanced implementations such as comparing different models. [Tuning Hyperparameters with Optuna](https://towardsdatascience.com/tuning-hyperparameters-with-optuna-af342facc549) has a series of canned alogrithms ready for use, including Lasso, Ridge, and Random Forest.
'76' [Git Branching - Basic Branching and Merging](https://git-scm.com/book/en/v2/Git-Branching-Basic-Branching-and-Merging) Quick start guide. Excellent!
'77' [Optuna+Keras Colab notebook](https://colab.research.google.com/github/Minyus/optkeras/blob/master/examples/OptKeras_Example.ipynb) contains a nice example for quick start.
'78' [Stacking XGBoost + LightGBM on GPU Optuna](https://www.kaggle.com/tomwarrens/stacking-xgboost-lgbm-on-gpu-optuna)
'79' [The exponential family: conjugate priors](https://people.eecs.berkeley.edu/~jordan/courses/260-spring10/other-readings/chapter9.pdf) is an excellent material for reviewing Bayes for interview. It contaisn the derivations of posterior distribution for classification (binary or multinomial) and regression problems. Definition of conjugate pairs can be found in [wiki](https://en.wikipedia.org/wiki/Conjugate_prior), which says "The exact interpretation of the parameters of a beta distribution in terms of number of successes and failures depends on what function is used to extract a point estimate from the distribution.." More deatils regarding the derivation of Bayes Gaussian distribution. ![](https://i.postimg.cc/nrrpJD29/2021-03-19-at-22-27-28.png)
'80' [Sklearn Pipeline : pass a parameter to a custom Transformer?](https://stackoverflow.com/questions/55142677/sklearn-pipeline-pass-a-parameter-to-a-custom-transformer) mentions the way to introduce parameters using `__init__()`. '81' [How to set values in a conjugate prior](https://tinyurl.com/yjdkqdoy).
'81' [Likelihoood encoding of categorical features](https://www.kaggle.com/tnarik/likelihood-encoding-of-categorical-features) 很好的雙重CV的解釋，尤其是apply double CV on categorical feature encoding to avoid information leakage. Good for interview.
'82' [詳細的random forest, lgithGBM, and XGBoost比較](https://zhuanlan.zhihu.com/p/33700459) ![](https://i.postimg.cc/NF4F3Nk4/2021-03-25-at-16-32-53.png) Good for interview. ![](https://i.postimg.cc/3NS4yS0w/2021-03-25-at-16-37-04.png)
'83' [Demystifying Maths of Gradient Boosting](https://towardsdatascience.com/demystifying-maths-of-gradient-boosting-bd5715e82b7c) Nice! [Math 1](https://www.digdeepml.com/2020/04/04/Math-Behind-GBM-and-XGBoost/), [Math 2](https://medium.com/analytics-vidhya/math-behind-gbm-and-xgboost-d00e8536b7de), [Math 3](https://www.kdnuggets.com/2018/08/unveiling-mathematics-behind-xgboost.html).



Software engineering
1. In the ipynb "K-fold-Target-Encoding.ipynb," adopt its CV block, CV merge block and test data updating block.
2. The "Optuna_ridgeClf_demo.py" file contains a full example that integrates Optuna with Pipelines and ColumnTransformer. The groups of columns passed into the pipeline are made dynamically in the sense that we can group columns into subgrups as many as we prefer, byeond a common setting that we splits features into a numerical features group and a categorical features group.
3. The 'gaussian_target_encoder.py' file includes an important demo of hierachical bayesian categorical encoding.
4. The 'categorical encoder module.py' file is the official categorical encoder source code. It contains smoothing to alleviate overfitting but it didn't consider CV and cannot work with hierachical features system.


[the use of cross val score to return values to the objective function](https://brianmusisi.com/design/Predicting+House+Prices-2.html)
[repeatedStratifiedKFold](https://scikit-learn.org/stable/modules/generated/sklearn.model_selection.RepeatedStratifiedKFold.html)
[Neither normalization nor binning is needed for xgboost and random forest in general](https://www.reddit.com/r/analytics/comments/d25wht/when_are_data_normalization_and_data_binning/) 我認為binning reduces information (noise) may be helpful for generalization.
